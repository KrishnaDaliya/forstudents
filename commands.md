Commands to use Git.

-------------------------------------------------------------------------
-------------------------------------------------------------------------

Check version or availability
--> git --version (check your current version of Git)

-------------------------------------------------------------------------

Initialize local repository in a directory
--> git init

-------------------------------------------------------------------------

Set your username:
--> git config --global user.name "FIRST_NAME LAST_NAME"

Set your email address:
--> git config --global user.email "MY_NAME@example.com"

Check username and email address
--> git config --global --list

-------------------------------------------------------------------------

Display the state of the working directory and the staging area
--> git status

-------------------------------------------------------------------------

Selects that file, and moves it to the staging area, marking it for inclusion in the next commit
--> git add <file name> (stage a specific file or directory)
--> git add . (stage all files)

-------------------------------------------------------------------------

Upload local repository content to a remote repository 
--> git push -u "URL" <branch> (for initial push to remote repository)
--> git push (once your local repository is connected with to remote repo)

--------------------------------------------------------------------------

Update the local version of a repository from a remote
--> git pull "URL" 

--------------------------------------------------------------------------

Utility tool to review and read a history of everything that happens to a repository
--> git log

--------------------------------------------------------------------------

List all of the branches in your repository
--> git branch or git branch --list 



